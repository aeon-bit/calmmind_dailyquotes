import 'package:flutter/material.dart';

var shadColor = const Color.fromARGB(100, 107, 127, 192);

var lGradient1 = const LinearGradient(
    begin: Alignment(-0.9, -3),
    end: Alignment(0.7, 3.5),
    colors: [Color(0xff1d7ddf), Color(0xff7edce9)],
    stops: [0.05, 1]);
var lGradient2 = const LinearGradient(
    begin: Alignment(-0.9, -3),
    end: Alignment(0.7, 3.5),
    colors: [Color(0xffFF9090), Color(0xffFDDCDC)],
    stops: [0.05, 1]);
var lGradient3 = const LinearGradient(
    begin: Alignment(-0.9, -3),
    end: Alignment(0.7, 3.5),
    colors: [Color(0xff2787E0), Color(0xff7993E7)],
    stops: [0.05, 1]);

var gBoxShad = BoxShadow(
  color: shadColor,
  offset: Offset(0, 14),
  blurRadius: 10,
  spreadRadius: -7,
);

List listHomeTitle = [
  'General',
  'Favourite',
  'Motivation',
  'Love',
  'Deep',
  'Positive Thinking',
  'Relationship',
  'Mental Health',
  'Falling in Love',
  'Over Thingking',
  'Life',
  'Self Decipline',
  'Lesson',
  'Philosophy',
];

List listIcon = [
  'general.svg',
  'fav.svg',
  'motivation.svg',
  'love.svg',
  'deep.svg',
  'pos_think.svg',
  'relation.svg',
  'mental_healt.svg',
  'fall_love.svg',
  'over_think.svg',
  'life.svg',
  'self_decip.svg',
  'lesson.svg',
  'philos.svg',
];

//General
var listQuGeneral = [
  {
    'qu': 'Quote 1',
    'au': 'Au 1',
  },
  {
    'qu': 'Quote 2',
    'au': 'Au 2',
  },
];

//Motivation
List listQuMotivation = [
  {
    'qu':
        'Aku suka tidur. Kehidupanku memiliki kecenderungan untuk jatuh ketika aku sadar',
    'au': 'Ernest Hemingway',
  },
  {
    'qu':
        'Jika engkau ingin hidup senang, maka hendaklah engkau rela dianggap sebagai tidak berakal atau dianggap orang bodoh',
    'au': 'Phytagoras',
  },
  {
    'qu':
        'Jangan sekali-kali percaya pada kasih sayang yang datang tiba-tiba, karena dia akan meninggalkanmu dengan tiba-tiba pula',
    'au': 'Phytagoras',
  },
  {
    'qu':
        'Keberhasilan adalah kemampuan melewati dan mengatasi dari satu kegagalan ke kegagalan berikutnya tanpa kehilangan semangat',
    'au': 'Winston Chuchill',
  },
  {
    'qu':
        'Aku suka pada mereka yang masuk menemu malam. Malam yang berwangi mimpi, terlucut debu',
    'au': 'Chairil Anwar',
  },
  {
    'qu':
        'Kesakitan membuat Anda berpikir. Pikiran membuat Anda bijaksana. Kebijaksanaan membuat kita bertahan dalam hidup.',
    'au': 'John Pattrick',
  },
  {
    'qu':
        'Jangan membanggakan apa yang kamu lakukan hari ini, sebab engkau tidak akan tahu apa yang akan diberikan oleh hari esok',
    'au': 'Phytagoras',
  },
  {
    'qu':
        'Ada yang berubah, ada yang bertahan. Karena zaman tak bisa dilawan. Yang pasti kepercayaan harus diperjuangkan.',
    'au': 'Chairil Anwar',
  },
  {
    'qu':
        'Hidup hanya menunda kekalahan, tambah terasing dari cinta sekolah rendah, dan tahu, ada yang tetap tidak diucapkan, sebelum pada akhirnya kita menyerah.',
    'au': 'Chairil Anwar',
  },
  {
    'qu':
        'Aku berbenah dalam kamar, dalam diriku jika kau datang dan aku bisa lagi lepaskan kisah baru padamu.',
    'au': 'Chairil Anwar',
  },
  {
    'qu':
        'All blame is a waste of time. No matter how much fault you find with another, andregardless of how much you blame him, it will not change you',
    'au': 'Wayne Dyer',
  },
  {
    'qu':
        'Risk more than other think is safe. Care more than other think is wise. Dream more than other think is practical. Expect more than other think is possible',
    'au': 'Claude T. Bissell',
  },
  {
    'qu':
        'The man who does more than he is paid for will soon be paid more than he does',
    'au': 'Napoleon Hill',
  },
  {
    'qu':
        'Don`t ask yourself what the world needs, ask yourself what makes you come alive. And then go and to that. Because what the world needs is people who have come alive',
    'au': 'Harold Whitman',
  },
  {
    'qu':
        'Don`t give up when you still have something to give. Nothing is really over until the moment you stop trying',
    'au': 'Brian Dyson',
  },
  {
    'qu':
        'Do not take for granted the things closest to your heart. Cling to them as you would with your life, for without them, life is meaningless',
    'au': 'Chinese Proverbs',
  },
  {
    'qu':
        'Change hurts. It makes people insecure, confused, and angry. People want things to be the same as they`ve always been, because that makes life easier',
    'au': 'Richard Marcinko',
  },
  {
    'qu':
        'The only limit to our realization of tomorrow will be our doubts of today. Let us move forward with strong and active faith',
    'au': 'Franklin Roosevelt',
  },
  {
    'qu':
        'The weak can never forgive. Forgiveness is the attribute of the strong',
    'au': 'Mahatma Gandhi',
  },
  {
    'qu':
        'Develop the winning edge. Small differences in your performance can lead to large differences in your results',
    'au': 'Brian Tracy',
  },
  {
    'qu':
        'It`s a little like wrestling a gorilla. You don`t quit when you`re tired, you quit when the gorilla is tired',
    'au': 'Robert Strauss',
  },
  {
    'qu':
        'First we form habits then they form us. Conquer your bad habits, or they will eventually conquer you',
    'au': 'Dr. Rob Gilbert',
  },
  {
    'qu':
        'The gem cannot be polished with friction, nor man perfected without trials',
    'au': 'Chinese Proverbs',
  },
  {
    'qu':
        'The most basic and powerful way to connect to another person is to listen. Just listen. Perhaps the most important thing we ever give each other is our attention',
    'au': 'Rachel Naomi Remen',
  },
  {
    'qu':
        'Treat everyone with politeness, even those who are rude to you. Not because they are nice, but because you are',
    'au': 'Chinese Proverbs',
  },
  {
    'qu':
        'If you want something in your life you`ve never had. you`ll have to do something, you`venever done',
    'au': 'JD Houston',
  },
  {
    'qu': 'Where there is no struggle, there is no strength',
    'au': 'Oprah Winfrey',
  },
  {
    'qu':
        'Don`t let the noise of other`s opinions drown out your own inner voice',
    'au': 'Steve Jobs',
  },
];

import 'dart:async';
import 'dart:js';

import 'package:calm_mind_daily_quotes/pages/home.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

class Splashscreen extends StatefulWidget {
  // const Splashscreen({super.key});

  @override
  State<Splashscreen> createState() => _SplashscreenState();
}

class _SplashscreenState extends State<Splashscreen> {
  final Color beginGradient = const Color(0xFF1e7edf);
  final Color endGradient = const Color(0xFF7eddea);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    // startTime();
    Future.delayed(const Duration(milliseconds: 100)).then((value) => {
          Navigator.push(this.context,
              MaterialPageRoute(builder: ((context) => HomePage())))
        });
  }

  // startTime() async {
  //   var duration = new Duration(seconds: 5);
  //   return new Timer(duration, route);
  // }

  // route() {
  //   Navigator.push(this.context,
  //       MaterialPageRoute(builder: (context) => const HomePage()));
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [beginGradient, endGradient],
            begin: Alignment(0, -1),
            end: Alignment(0, 5),
          ),
        ),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                height: 80,
                padding: EdgeInsets.all(20),
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('assets/images/intro_icon.png'))),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                height: 60,
                padding: EdgeInsets.all(20),
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image:
                            AssetImage('assets/images/intro_icon_text.png'))),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

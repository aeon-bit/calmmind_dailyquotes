import 'package:calm_mind_daily_quotes/models/globData.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

class ViewQuote extends StatefulWidget {
  ViewQuote({this.index = 0});

  int index;

  @override
  State<ViewQuote> createState() => _ViewQuoteState();
}

class _ViewQuoteState extends State<ViewQuote> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        iconTheme: IconThemeData(
          color: Color(0xff8A8A8A),
        ),
      ),
      backgroundColor: Color(0xff373737),
      body: Column(
        children: [
          Expanded(
            child: Center(
              child: Text(
                listQuMotivation[widget.index]['qu'],
                style: TextStyle(
                  fontFamily: 'Poppins',
                  color: Colors.white,
                  fontSize: 18,
                  fontWeight: FontWeight.w200,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Container(
            height: AppBar().preferredSize.height,
            color: Colors.white.withAlpha(20),
          ),
        ],
      ),
    );
  }
}

import 'package:calm_mind_daily_quotes/models/globData.dart';
import 'package:calm_mind_daily_quotes/models/gobalvar.dart';
import 'package:calm_mind_daily_quotes/pages/view_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ListPage extends StatefulWidget {
  ListPage({
    super.key,
    this.id = 0,
  });

  int id;

  @override
  State<ListPage> createState() => _ListPageState();
}

class _ListPageState extends State<ListPage> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        iconTheme: const IconThemeData(
          color: Color.fromARGB(255, 255, 255, 255),
        ),
      ),
      body: Stack(
        children: <Widget>[
          Container(
            width: double.infinity,
            height: 160,
            // color: Colors.amber,
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/top_banner.png'),
                fit: BoxFit.fill,
              ),
            ),
          ),
          Column(
            children: <Widget>[
              Row(
                children: [
                  Expanded(
                    child: Container(
                      height: AppBar().preferredSize.height + 40,
                      child: Center(
                        child: Text(
                          listHomeTitle[widget.id - 1],
                          style: const TextStyle(
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.bold,
                            fontSize: 28,
                            color: Color.fromARGB(255, 255, 255, 255),
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Stack(
                    children: [
                      Container(
                        margin: const EdgeInsets.only(right: 20),
                        width: 80,
                        height: 80,
                        padding: const EdgeInsets.all(20),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.white,
                          boxShadow: [
                            gBoxShad,
                          ],
                        ),
                        child: SvgPicture.asset(
                            'assets/svgs/${listIcon[widget.id - 1]}'),
                      ),
                    ],
                  ),
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              MediaQuery.removePadding(
                context: context,
                removeTop: true,
                child: Expanded(
                  child: ListView.builder(
                    physics: const BouncingScrollPhysics(),
                    itemCount: listQuMotivation.length,
                    itemBuilder: (context, index) {
                      return ListCard(
                        index: index,
                      );
                    },
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class ListCard extends StatefulWidget {
  ListCard({
    this.index = 0,
    this.lMargin = 20,
    this.rMargin = 20,
  });

  final int index;
  double lMargin;
  double rMargin;

  @override
  State<ListCard> createState() => _ListCardState();
}

class _ListCardState extends State<ListCard> {
  @override
  void initState() {
    // TODO: implement initState
    if (widget.index.isOdd) {
      widget.rMargin = 70;
    } else {
      widget.lMargin = 70;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        left: widget.lMargin,
        right: widget.rMargin,
        bottom: 40,
      ),
      // padding: EdgeInsets.only(
      //   top: 35,
      //   bottom: 35,
      //   left: 50,
      //   right: 50,
      // ),
      width: MediaQuery.of(context).size.width,
      height: 120,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        boxShadow: [
          BoxShadow(
            color: Color.fromARGB(123, 107, 127, 192),
            offset: Offset(0, 14),
            blurRadius: 10,
            spreadRadius: -7,
          ),
        ],
        gradient: lGradient1,
      ),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => ViewQuote(
                index: widget.index,
              ),
            ));
          },
          focusColor: Colors.white,
          child: Container(
            padding: EdgeInsets.only(
              top: 35,
              bottom: 35,
              left: 50,
              right: 50,
            ),
            child: Column(
              children: [
                Text(
                  listQuMotivation[widget.index]['qu'],
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontFamily: 'Poppins',
                    fontWeight: FontWeight.w300,
                    color: Colors.white,
                  ),
                ),
                Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        '~ ${listQuMotivation[widget.index]['au']}',
                        style: TextStyle(
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.w100,
                          fontStyle: FontStyle.italic,
                          color: Colors.white,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

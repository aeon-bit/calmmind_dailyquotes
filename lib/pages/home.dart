import 'package:calm_mind_daily_quotes/home_widgets/card_home.dart';
import 'package:calm_mind_daily_quotes/models/globData.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var cardColor = Color(0xff6B7FC0);
  var cardTextStyle = TextStyle(fontFamily: 'Montserrat', color: Colors.white);

  @override
  void initState() {
    // TODO: implement initState
    // print(listQuGeneral[1]['qu']);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var wScreenSize = MediaQuery.of(context).size.width;
    var cardWidth = wScreenSize / 2 - 30;

    // List cardSvgs = [
    //   "assets/svgs/general.svg", //0
    //   "assets/svgs/fav.svg", //1
    //   "assets/svgs/motivation.svg", //2
    //   "assets/svgs/love.svg", //3
    //   "assets/svgs/deep.svg", //4
    //   "assets/svgs/pos_think.svg", //5
    //   "assets/svgs/relation.svg", //6
    //   "assets/svgs/mental_healt.svg", //7
    //   "assets/svgs/fall_love.svg", //8
    //   "assets/svgs/over_think.svg", //9
    //   "assets/svgs/life.svg", //10
    //   "assets/svgs/self_decip.svg", //11
    //   "assets/svgs/lesson.svg", //12
    //   "assets/svgs/philos.svg", //13
    // ];

    // List cardTitle = [
    //   'General',
    //   'Favourite',
    //   'Motivation',
    //   'Love',
    //   'Deep',
    //   'Positive Thingking',
    //   'Relationship',
    //   'Mental Healt',
    //   'Falling In Love',
    //   'Over Thingking',
    //   'Life',
    //   'Self Descipline',
    //   'Lesson',
    //   'Philosophy',
    // ];

    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
        body: Container(
            padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
            child: SingleChildScrollView(
              physics: const BouncingScrollPhysics(),
              child: Column(
                children: <Widget>[
                  const Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        'Categories',
                        style: TextStyle(
                          fontFamily: 'Montserrat',
                          fontWeight: FontWeight.bold,
                          fontSize: 28,
                          color: Color(0xff373737),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  const SizedBox(
                    height: 40,
                    child: CupertinoSearchTextField(
                      style: TextStyle(
                        fontSize: 12,
                        fontFamily: 'Montserrat',
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 40,
                  ),
                  Row(
                    //1 CARD BUTTON
                    children: [
                      CardHome(
                        cardWidth: cardWidth,
                        cardIcon: 'assets/svgs/${listIcon[0]}',
                        cardTitle: listHomeTitle[0],
                        cardNum: 1,
                        id: 1,
                      ),
                      const Gap(20),
                      CardHome(
                        cardWidth: cardWidth,
                        cardIcon: 'assets/svgs/${listIcon[1]}',
                        cardTitle: listHomeTitle[1],
                        cardNum: 2,
                        id: 2,
                      ),
                    ],
                  ),

                  //Line
                  Row(
                    children: [
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(top: 20, bottom: 20),
                          height: 2,
                          color: Color(0xff434E71).withOpacity(0.1),
                        ),
                      ),
                    ],
                  ),

                  Row(
                    //2 CARD BUTTON
                    children: [
                      CardHome(
                        cardWidth: cardWidth,
                        cardIcon: 'assets/svgs/${listIcon[2]}',
                        cardTitle: listHomeTitle[2],
                        cardNum: 0,
                        id: 3,
                      ),
                      const Gap(20),
                      CardHome(
                        cardWidth: cardWidth,
                        cardIcon: 'assets/svgs/${listIcon[3]}',
                        cardTitle: listHomeTitle[3],
                        cardNum: 0,
                        id: 4,
                      ),
                    ],
                  ),
                  Row(
                    //3 CARD BUTTON
                    children: [
                      CardHome(
                        cardWidth: cardWidth,
                        cardIcon: 'assets/svgs/${listIcon[4]}',
                        cardTitle: listHomeTitle[4],
                        cardNum: 0,
                        id: 5,
                      ),
                      Gap(20),
                      CardHome(
                        cardWidth: cardWidth,
                        cardIcon: 'assets/svgs/${listIcon[5]}',
                        cardTitle: listHomeTitle[5],
                        cardNum: 0,
                        id: 6,
                      ),
                    ],
                  ),
                  Row(
                    //2 CARD BUTTON
                    children: [
                      CardHome(
                        cardWidth: cardWidth,
                        cardIcon: 'assets/svgs/${listIcon[6]}',
                        cardTitle: listHomeTitle[6],
                        cardNum: 0,
                        id: 7,
                      ),
                      Gap(20),
                      CardHome(
                        cardWidth: cardWidth,
                        cardIcon: 'assets/svgs/${listIcon[7]}',
                        cardTitle: listHomeTitle[7],
                        cardNum: 0,
                        id: 8,
                      ),
                    ],
                  ),
                  Row(
                    //2 CARD BUTTON
                    children: [
                      CardHome(
                        cardWidth: cardWidth,
                        cardIcon: 'assets/svgs/${listIcon[8]}',
                        cardTitle: listHomeTitle[8],
                        cardNum: 0,
                        id: 9,
                      ),
                      Gap(20),
                      CardHome(
                        cardWidth: cardWidth,
                        cardIcon: 'assets/svgs/${listIcon[9]}',
                        cardTitle: listHomeTitle[9],
                        cardNum: 0,
                        id: 10,
                      ),
                    ],
                  ),
                  Row(
                    //2 CARD BUTTON
                    children: [
                      CardHome(
                        cardWidth: cardWidth,
                        cardIcon: 'assets/svgs/${listIcon[10]}',
                        cardTitle: listHomeTitle[10],
                        cardNum: 0,
                        id: 11,
                      ),
                      Gap(20),
                      CardHome(
                        cardWidth: cardWidth,
                        cardIcon: 'assets/svgs/${listIcon[11]}',
                        cardTitle: listHomeTitle[11],
                        cardNum: 0,
                        id: 12,
                      ),
                    ],
                  ),
                  Row(
                    //2 CARD BUTTON
                    children: [
                      CardHome(
                        cardWidth: cardWidth,
                        cardIcon: 'assets/svgs/${listIcon[12]}',
                        cardTitle: listHomeTitle[12],
                        cardNum: 0,
                        id: 13,
                      ),
                      Gap(20),
                      CardHome(
                        cardWidth: cardWidth,
                        cardIcon: 'assets/svgs/${listIcon[13]}',
                        cardTitle: listHomeTitle[13],
                        cardNum: 0,
                        id: 14,
                      ),
                    ],
                  ),
                ],
              ),
            )),
      ),
    );
  }
}

import 'package:calm_mind_daily_quotes/pages/list_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CardHome extends StatefulWidget {
  final double cardWidth;
  final String cardIcon, cardTitle;
  final int cardNum, id;

  const CardHome({
    super.key,
    this.cardWidth = 100.0,
    this.cardIcon = "assets/svgs/general.svg",
    this.cardTitle = "Title",
    this.cardNum = 0,
    this.id = 0,
  });

  @override
  State<CardHome> createState() => _CardHomeState();
}

class _CardHomeState extends State<CardHome> {
  var cardTextStyle, lGradient;
  var cardColor1a = const Color(0xff1d7ddf);
  var cardColor1b = const Color(0xff7edce9);
  var cardColor2a = const Color(0xffFF9090);
  var cardColor2b = const Color(0xffFDDCDC);
  var cardColor3a = const Color(0xff2787E0);
  var cardColor3b = const Color(0xff7993E7);
  var shadColor = const Color.fromARGB(100, 107, 127, 192);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    if (widget.cardNum == 1) {
      //blue
      lGradient = LinearGradient(
        begin: Alignment(-0.9, -3),
        end: Alignment(0.7, 3.5),
        colors: [cardColor1a, cardColor1b],
        stops: [0.05, 1],
      );
    } else if (widget.cardNum == 2) {
      //red
      lGradient = LinearGradient(
        begin: Alignment(-0.9, -3),
        end: Alignment(0.7, 3.5),
        colors: [cardColor2a, cardColor2b],
        stops: [0.05, 1],
      );
    } else {
      lGradient = LinearGradient(
        //purple
        begin: Alignment(-0.9, -3),
        end: Alignment(0.7, 3.5),
        colors: [cardColor3a, cardColor3b],
        stops: [0.05, 1],
      );
    }

    cardTextStyle = const TextStyle(
      fontFamily: 'Inter',
      color: Colors.white,
      fontSize: 14,
      fontWeight: FontWeight.w600,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        top: 10,
        bottom: 10,
      ),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          gradient: lGradient,
          boxShadow: [
            BoxShadow(
              color: shadColor,
              offset: Offset(0, 14),
              blurRadius: 10,
              spreadRadius: -7,
            ),
          ]),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ListPage(
                    id: widget.id,
                  ),
                ));
          },
          focusColor: Colors.white,
          child: Container(
            // margin: EdgeInsets.only(top: 10, bottom: 10),
            width: widget.cardWidth,
            height: 80,
            padding: const EdgeInsets.all(6),
            child: Row(
              children: [
                Expanded(
                  flex: 3,
                  child: Column(
                    children: [
                      Container(
                        alignment: Alignment.centerLeft,
                        padding: const EdgeInsets.only(top: 10, left: 10),
                        child: Text(
                          widget.cardTitle,
                          style: cardTextStyle,
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Container(
                    padding: const EdgeInsets.only(top: 16, bottom: 16),
                    height: double.infinity,
                    child: SvgPicture.asset(widget.cardIcon),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
